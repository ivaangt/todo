function applyDiscount(vehicles, discount){
    return new Promise((resolve, reject) => {
       if(typeof discount !== 'number') {
           throw new Error('Invalid discount');
       }
       if(vehicles.find(vehicle => typeof vehicle.make !== 'string' || typeof vehicle.price !== 'number')) {
           throw new Error('Invalid array format');
       }
       //Math.min merge apelat pe mai multe valori si returneaza minimul, dar nu pe arrays
       //asa ca el foloseste spread operator (...) 
       //(...) iti ia practic doar valorile din array, care array la el este un map de vehicles.
       //adica el preia doar preturile din arrayul de obiecte..apoi face minimul
       //PS:ne-a zis ca testul e gresit trebuie 25000 in loc de 23000
       const min = Math.min(...vehicles.map(vehicle => vehicle.price));
       if(discount > min / 2) {
           throw new Error('Discount too big');
       }
       //aici creeaza un map in care modifica doar pretul
       //daca functia map() are mai mult de 1 linie de cod..
       //trebuie facuta ca lamda function cu acolade si cu return 
       const result = vehicles.map(vehicle => {
           vehicle.price -= discount;
           return vehicle;
       })
       resolve(result);
    })   
}

const app = {
    applyDiscount: applyDiscount
};

module.exports = app;