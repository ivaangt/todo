const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.students = [
    {
        name: "Gigel",
        surname: "Popel",
        age: 23
    },
    {
        name: "Gigescu",
        surname: "Ionel",
        age: 25
    }
];

app.get('/students', (req, res) => {
    res.status(200).json(app.locals.products);
});

app.post('/students', (req, res, next) => {
    let student=req.body;
    
    if(Object.keys(student).length<=0)
    {
        res.status(500).json({message: "Body is missing"});
    }
    else
    {
        if(!student.hasOwnProperty("name")||!student.hasOwnProperty("surname")||!student.hasOwnProperty("age"))
        {
            res.status(500).json({message: "Invalid body format"});
        }
        else
        {
            if(student.age<0)
            {
                 res.status(500).json({message: "Age should be a positive number"});
            }
            else
            {
                //daca find gaseste studentul returneaza primul obiect care indeplineste conditia, altfel returneaza undefined 
               if(app.locals.students.find(stud=>stud.name===student.name))
               {
                    res.status(500).json({message: "Student already exists"});
               }
               else
               {
                   app.locals.students.push(student);
                   res.status(201).json({message: "Created"})
               }
            }
        }
   
    }
    // res.status(400).json({'message': 'Bad request'});
})

module.exports = app;