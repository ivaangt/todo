// function bowdlerize(input, dictionary){
//     var s="";
    
//     if(typeof(s)!==typeof(input))
//     {
//         throw new Error("Input should be a string");
//     }
//     else
//     {
//         for(var i=0;i<dictionary.length;i++)
//         {
//             if(typeof(s)!==typeof(dictionary[i]))
//             {
//                 throw new Error("Invalid dictionary format");
//             }
            
//         }
//     var input_words=new Array();
//     input_words=input.split(" ");
//     var returnedInput="";
//     for(var i=0;i<input_words.length;i++)
//     {
//       for(var j=0;j<dictionary.length;j++)
//       {
//           //inseamna ca un cuvant se regaeeste in dictionar
//           if(input_words[i]===dictionary[j] ||
//           input_words[i].toUpperCase()===dictionary[j].toUpperCase())
//           {
//               //steleute=cate litere are cuvantu-2
//               var no=dictionary[j].length-2;
//               var result=""
//               for(var k=0;k<no;k++)
//               {
//                   result+="*";
//               }
             
//               var firstLetter = input_words[i].slice(0,1);
//               var lastLetter = input_words[i].slice(-1);
//               input_words[i]=`${firstLetter}${result}${lastLetter}`;
//           }
//       }
//       returnedInput+=input_words[i]+" ";
//     }
    
//     }
    
//     return returnedInput.trim();

// }

// const app = {
//     bowdlerize
// };

// module.exports = app;


//sssssaaaaaaaauuuuuuu

function bowdlerize(input, dictionary)
{
    if(typeof input !== "string")
    {
        throw new Error("Input should be a string")
    }
    
    dictionary.forEach(word => 
    {
        if(typeof word !== "string")
        {
            throw new Error("Invalid dictionary format")
        }
    })
    
    dictionary.forEach(dict =>
    {
        input.split(" ").forEach(word =>
        {
            if(dict === word.toLowerCase())
            {
                input = input.replace(word, word[0]+"*".repeat(word.length-2)+word[word.length-1])
                return input
            }
        })
    })
    return input
}

const app = {
    bowdlerize
};

module.exports = app;


//--sauuu



function bowdlerize(input, dictionary) {
  if (typeof input != "string") {
    throw new Error("Input should be a string");
  }
  dictionary.forEach(word => {
    if (typeof word != "string") {
      throw new Error("Invalid dictionary format");
    }
  });

  let sentence = input.split(" ");

  let censored = input;

  dictionary.forEach(wordD => {
    sentence.forEach(wordS => {
        let lowercase = wordS.toLowerCase();
      if (wordD === wordS || wordD === lowercase) {
       
        let newWord = wordS[0]+"*".repeat(wordS.length-2)+wordS[wordS.length-1];
      
        censored = censored.replace(wordS,newWord);
      }
    });
  });
  return censored;
}

const app = {
    bowdlerize
};

module.exports = app;