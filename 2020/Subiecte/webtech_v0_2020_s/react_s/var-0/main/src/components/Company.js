------------------------fara form---------------------------------

import React, { Component } from 'react'

class Company extends Component{
    
     handleClickButton = (event)=>{
        this.props.onDelete(this.props.item.id);
    }
    
    
    render(){
        return(
            <div>
                "Company {this.props.item.name}" 
                 <input type = "button" value = "delete" onClick = { this.handleClickButton}/>
            </div>
            )
    }
}

export default Company













--------------------------------------------cu form---------------------------


import React, { Component } from 'react'
class Company extends Component{
    constructor(props){
        super(props)
        this.state = {
            isEditing : false,
            name : this.props.item.name,
            color : this.props.item.color
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                name : this.state.name,
                color : this.state.color
            })
            this.setState({
                isEditing : false
            })
        }
        this.edit = () => {
            this.setState({
                isEditing : true
            })
        }
        this.cancel = () => {
            this.setState({
                isEditing : false
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item)
        }
    }
    render(){
        let {item} = this.props
        if (this.state.isEditing){
            return <div>
                <h4>
                    <input type="text" value={this.state.name} name="name" onChange={this.handleChange} />
                </h4>
                <h5>color : 
                    <input type="text" value={this.state.color} name="color"  onChange={this.handleChange} />
                </h5>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else{
            return <div>
                <h4>{item.name}</h4>
                <h5>color : {item.color}</h5>
                <div>
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="select self" onClick={this.select} />
                </div>
            </div>
        }
    }
}

export default Company