import React, {Component} from 'react'
class CompanyForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            color : ''
        }
        this.add = (evt) => {
            this.props.onAdd({
                name : this.state.name,
                color : this.state.color
            })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="name" name="name" onChange={this.handleChange} />
            <input type="text" placeholder="color" name="color" onChange={this.handleChange} />
            <input type="button" value="+" onClick={this.add} />
        </div>
    }
}

export default CompanyForm