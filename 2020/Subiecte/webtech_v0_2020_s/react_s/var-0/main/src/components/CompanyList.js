---------------fara form---------------------------
import React, { Component } from 'react'
import CompanyStore from '../stores/CompanyStore'
import Company from "./Company";

class CompanyList extends Component {
	constructor(){
		super()
		this.state = {
			companies : []
		}
		this.deleteCompany = (id) => {
			this.store.deleteOne(id)
		}
	}
	componentDidMount(){
		this.store = new CompanyStore()
		this.setState({
			companies : this.store.getAll()
		})
		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				companies : this.store.getAll()
			})			
		})
	}
  render() {
  	const companies= this.state.companies.map((comp,index) =>{
  		return <Company item ={comp}  key = {index} onDelete = {this.deleteCompany}/>
  	})
    return (
      <div>
      	{companies}
      </div>
    )
  }
}

export default CompanyList

-------------------cu form--------------------

import React, { Component } from 'react'
import CompanyStore from '../stores/CompanyStore'
import Company from './Company'
import CompanyForm from './CompanyForm'
class CompanyList extends Component {
    constructor(){
        super()
        this.state = {
            companies : [],
            selectedCompany : null
        }
        	this.store = new CompanyStore()
        this.addCompany = (company) => {
						this.store.addOne(company)
						}
        this.deleteCompany = (id) => {
            this.store.deleteOne(id)
        }
        this.saveCompany = (id, Company) => {
            this.store.saveCompany(id, Company)
        }
        this.selectCompany = (Company) =>{
            this.setState({
                selectedCompany : Company
            })
        }
        this.cancelSelection = () => {
            this.setState({
                selectedCompany : null
            })
        }
    }
	componentDidMount(){
		this.setState({
			companies : this.store.getAll()
		})
		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				companies : this.store.getAll()
			})			
		})
	}
    render(){
        return <div>
                <div>
                    {
                        this.state.companies.map((e, i) => <Company key={i} item={e} onDelete={this.deleteCompany} onSave={this.saveCompany} onSelect={this.selectCompany} />)
                    }
                </div>
                <CompanyForm onAdd={this.addCompany} />
            </div>             
        
    }
}

export default CompanyList

