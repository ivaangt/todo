/*
 - funcția distance primește ca parametrii două array-uri
 - fiecare element poate apărea cel mult o dată într-un array; orice apariții suplimentare sunt ignorate 
 - distanța dintre cele 2 array-uri este numărul de elemente diferite dintre ele
 - dacă parametrii nu sunt array-uri se va arunca o excepție ("InvalidType")
*/
/*
 - the distance function receives as parameters two arrays
 - each element can appear in each array at most once; any duplicates are ignored
 - the distance between the 2 arrays is the number of different elements between them
 - if the parameters are not arrays an exception is thrown ("InvalidType")
*/

function distance(first, second)
{
	var ar=[];
	var counter=0;
	if(typeof(first)!==typeof(ar)||typeof(second)!==typeof(ar))
	{
		throw Error("InvalidType");
		
	}
	
		if(first.length===0&&second.length===0)
		{
			return counter;
		}
		var fArr = first.filter(function(item, pos, self) {
			return self.indexOf(item) == pos;
		});
		var sArr=second.filter(function(item, pos, self) {
			return self.indexOf(item) == pos;
		});	
		var fArr2=fArr.filter(f => !sArr.includes(f));
		var	sArr2=sArr.filter(f => !fArr.includes(f));
	
		var res=[...fArr2,...sArr2];
		
		return res.length;
}


module.exports.distance = distance