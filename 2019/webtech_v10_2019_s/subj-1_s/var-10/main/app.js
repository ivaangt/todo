function processString(input){
    
    if(input == ""){
        return 100;
    }
    
    var splittedInput = input.split(" ");
    var ok = 1;
    splittedInput.forEach(inp =>{
        if(isNaN(inp)){
            ok=0;
        }
    })
    if(ok==0){
        throw new Error("Item is not a number");
    }else{
        var sum = 0;
        splittedInput.forEach(inp =>{
            if(inp%2==0){
                sum+= parseFloat(inp);
            }
        })
        return 100 - sum;
    }
}


const app = {
    processString: processString
}

module.exports = app