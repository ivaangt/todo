import React, { Component } from 'react'

// TODO : adăugați posibilitatea de a edita un robot 
// editarea se face prin intermediul unui robot cu 2 stări, una de vizualizare și una de editare

// TODO : add the posibility to edit a robot 
// editing is done via 2 states a view state and an edit state

class Robot extends Component {
	constructor(props){
		super(props)
		this.state = {
			name: this.props.item.name,
			type: this.props.item.type,
			mass: this.props.item.mass,
			isEditing: false
		}
		
		this.handleChange = (evt) => {
			this.setState({
				[evt.target.name] : evt.target.value
			})
		}
		
		this.edit = () => {
			this.setState({isEditing:true});
		}
		
		this.save = () =>{
			this.setState({isEditing:false});
			this.props.onSave(this.props.item.id, {
				name: this.state.name,
				type: this.state.type,
				mass: this.state.mass
			})
		}
		
		this.cancel = () => {
			this.setState({isEditing:false})
		}
		
		
	}
	render() {
		let {item} = this.props
		if(this.state.isEditing){
			return(
				<div>
					<input type="text" name="name" id="name" value={this.state.name} onChange={this.handleChange}/> 
	                <input type="text" name="type" id="type" value={this.state.type} onChange={this.handleChange}/> 
	                <input type="text" name="mass" id="mass" value={this.state.mass}  onChange={this.handleChange}/> 

					<input type="button" value="save"  onClick={this.save}/> 
					<input type="button" value="cancel"  onClick={this.cancel}/> 


				</div>
				)
		} else{
			return (
				<div>
					Hello, my name is {item.name}. I am a {item.type} and weigh {item.mass}
					<input type="button" value="edit"  onClick={this.edit}/> 

				</div>

			)
		}
	}
}

export default Robot
