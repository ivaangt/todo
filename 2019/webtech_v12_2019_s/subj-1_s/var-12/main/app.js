class Shape{
	constructor(dimensions){
		this.dimensions = dimensions

	}
	area(){
		throw Error('not implemented');
	}
	
}

// TODO: Square, Circle, Rectangle
class Square extends Shape{
	constructor(dimensions,width)
	{
		super(dimensions);
		var {width:w}=dimensions;
		this.width=w;
	
	
	}
	area()
	{
		console.log(this.width);
		return this.width*this.width;
	}
	
}
class Circle extends Shape{
	constructor(dimensions,radius)
	{
		super(dimensions);
		var {radius:r}=dimensions;
		this.radius=r;
		
	}

	area()
	{
		return parseInt(Math.PI*this.radius*this.radius);
	}
}
class Rectangle extends Shape{
	constructor(dimensions,width,height)
	{
		super(dimensions);
		var {width:w}=dimensions;
		var {height:h}=dimensions;
		this.width=w;
		this.height=h;
	
	}
	area()
	{
		return this.width*this.height;
	}
}
const app = {
  Shape: Shape,
  Square : Square,
  Circle : Circle,
  Rectangle : Rectangle
}

module.exports = app