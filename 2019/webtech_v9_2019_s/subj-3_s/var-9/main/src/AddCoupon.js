import React from 'react';

export class AddCoupon extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            category: '',
            discount: '',
            availability: ''
        };
        
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }

    addCoupon = () => {
        let coupon = {
            category: this.state.category,
            discount: this.state.discount,
            availability: this.state.availability
        };
        this.props.onAdd(coupon);
    }

    render(){
        return(
            <div>
                <input type="text" placeholder="category" name="category" id="category" onChange={this.handleChange} />
            	<input type="text" placeholder="discount" name="discount" id="discount" onChange={this.handleChange} />
            	<input type="text" placeholder="availability" name="availability" id="availability" onChange={this.handleChange} />
            	<input type="button" value="add coupon" onClick={this.addCoupon} />
            </div>
        )
    }
}

export default AddCoupon