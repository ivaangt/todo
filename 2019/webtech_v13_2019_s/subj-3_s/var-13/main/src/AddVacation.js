import React from 'react';

 class AddVacation extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            destination: '',
            locationType: '',
            price: 0
           
        };
        
    this.handleChange = (evt) => {
        this.setState({
            [evt.target.name] : evt.target.value
            })
        }
    }
     handleAdd = () => {
        let item = {...this.state};
        this.props.itemAdded(item);
    }

    render(){
        return (
        <div>
        <input type='text' id='vacation-destination' name='destination' placeholder='destination'  onChange={this.handleChange}/>
        <input type='text' id='vacation-location-type' name='locationType' placeholder='locationType'  onChange={this.handleChange}/>
        <input type='number' id='vacation-price' name='price' placeholder='price'  onChange={this.handleChange}/>
        <input type='button' name='price' placeholder='price' value='add vacation' onClick={this.handleAdd}/>
        </div>
        );
    }

   
}
export default AddVacation;